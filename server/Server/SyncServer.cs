namespace Networking;

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

class SynchrServer
{
    public static void StartServer()
    {
        // Host Adresse, localhost oder IP 127.0.0.1
        IPHostEntry host = Dns.GetHostEntry("localhost");
        // List falls mehrere Adressen
        IPAddress ipAddress = host.AddressList[0];
        // IP und Port
        IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 8080);

        bool listening = true;

        try {
            using Socket listener = new Socket(
                ipAddress.AddressFamily, 
                SocketType.Stream, 
                ProtocolType.Tcp);
            listener.Bind(localEndPoint);
            // Maximal 10 requests 
            listener.Listen(10);
            Console.WriteLine($"Listen on {host.AddressList[0]}");
            Console.WriteLine("Waiting for connection...");
            Socket handler = listener.Accept();

             // Daten von Client
             string data = null;
             byte[] bytes = null;

            while (listening)
            {
                bytes = new byte[1024];
                int bytesRec = handler.Receive(bytes);
                data = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                Console.WriteLine("Server received : {0}", data);
                Console.WriteLine("Enter message:");
                byte[] msg = Encoding.ASCII.GetBytes(Console.ReadLine());
                var str = System.Text.Encoding.Default.GetString(msg);
                if(str == "q")
                    break;
                int bytesSent = handler.Send(msg);
                if (data.IndexOf("<EOF>") > -1)
                {
                    break;
                }
                data = null;
            }
            handler.Shutdown(SocketShutdown.Both);
            handler.Close();
        }
        catch (ArgumentNullException ane)
        {
            Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
        }
        catch (SocketException se)
        {
            Console.WriteLine("SocketException : {0}", se.ToString());
        }
        catch (Exception e)
        {
            Console.WriteLine("Unexpected exception : {0}", e.ToString());
        }
 
        Console.WriteLine("\n Press any key to continue...");
        Console.ReadKey();
    }
}