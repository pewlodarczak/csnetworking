namespace Networking;

using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

class SynchrClient
{
    public static void StartClient()
    {
        byte[] bytes = new byte[1024];
        try
        {
            IPHostEntry host = Dns.GetHostEntry("localhost");
            IPAddress ipAddress = host.AddressList[0];
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 8080);

            // TCP socket
            using Socket sender = new Socket(
                ipAddress.AddressFamily,
                SocketType.Stream, 
                ProtocolType.Tcp);

            try
            {
                sender.Connect(remoteEP);
                Console.WriteLine("Socket connected to {0}",
                    sender.RemoteEndPoint.ToString());
                

                //byte[] firstmsg = Encoding.ASCII.GetBytes("Hello from cowboy 👋");
                byte[] firstmsg = Encoding.ASCII.GetBytes("Hello from cowboy");
                int bytesSent = sender.Send(firstmsg);
                while(true) 
                {
                    Console.WriteLine("Enter message:");
                    byte[] msg = Encoding.ASCII.GetBytes(Console.ReadLine());
                    var str = System.Text.Encoding.Default.GetString(msg);
                    if(str == "q")
                        break;
                    bytesSent = sender.Send(msg);
                    int bytesRec = sender.Receive(bytes);
                    Console.WriteLine("Client received: {0}",
                        Encoding.ASCII.GetString(bytes, 0, bytesRec));
                }
                sender.Shutdown(SocketShutdown.Both);
                sender.Close();
            }
            catch (ArgumentNullException ane)
            {
                Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected exception : {0}", e.ToString());
            }
            finally {

            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString());
        }
    }

    public static void Sending(Socket sock)
    {
        byte[] firstmsg = Encoding.ASCII.GetBytes("Hello from cowboy");
        int bytesSent = sock.Send(firstmsg);
        while(true) 
        {
            Console.WriteLine("Enter message:");
            byte[] msg = Encoding.ASCII.GetBytes(Console.ReadLine());
            var str = System.Text.Encoding.Default.GetString(msg);
            if(str == "q")
                break;
            //Console.WriteLine("Message: " + System.Text.Encoding.Default.GetString(msg));
            bytesSent = sock.Send(msg);
        }
    }

    public static void Receiving(Socket sender)
    {
        byte[] bytes = new byte[1024];
        while(true) 
        {
            int bytesRec = sender.Receive(bytes);
            Console.WriteLine("Client received: {0}",
                Encoding.ASCII.GetString(bytes, 0, bytesRec));
        }
    }

}