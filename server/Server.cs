﻿using Networking;

using System.Threading;

class Server
{
    static void Main(String[] args)
    {
        //SynchrServer.StartServer();
        //var task = AsyncServer.StartServer();
        //var result = task.Result;

        Thread t = new Thread(delegate ()
        {
            // replace the IP with your system IP Address...
            MTServer server = new MTServer("127.0.0.1", 8080);
        });
        t.Start();
        Console.WriteLine("Server Started...!");
    }
}