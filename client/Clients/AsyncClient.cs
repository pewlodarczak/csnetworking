using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

class AsyncClient
{
    public async static Task<int> StartClient()
    {
        byte[] bytes = new byte[1024];
        //try
        //{
            IPHostEntry host = Dns.GetHostEntry("localhost");
            IPAddress ipAddress = host.AddressList[0];
            IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, 8080);

            // TCP socket
            using Socket client = new(
                ipEndPoint.AddressFamily, 
                SocketType.Stream, 
                ProtocolType.Tcp);

            await client.ConnectAsync(ipEndPoint);
            // while (true)
            // {
            //     // Send message.
            //     //var message = "Hi friends 👋!<|EOM|>";
            //     var message = "Hi friends <|EOM|>";
            //     var messageBytes = Encoding.UTF8.GetBytes(message);
            //     _ = await client.SendAsync(messageBytes, SocketFlags.None);
            //     Console.WriteLine($"Socket client sent message: \"{message}\"");

            //     // Receive ack.
            //     var buffer = new byte[1_024];
            //     var received = await client.ReceiveAsync(buffer, SocketFlags.None);
            //     var response = Encoding.UTF8.GetString(buffer, 0, received);
            //     if (response == "<|ACK|>")
            //     {
            //         Console.WriteLine(
            //             $"Socket client received acknowledgment: \"{response}\"");
            //         break;
            //     }
            // }
            byte[] msg = Encoding.ASCII.GetBytes("");
            int bytesSent = client.Send(msg);
            while(true) 
            {
                //msg = Encoding.ASCII.GetBytes("Hello from cowboy");
                bytesSent = client.Send(msg);
                Console.WriteLine("Enter message:");
                msg = Encoding.ASCII.GetBytes(Console.ReadLine());
                var str = System.Text.Encoding.Default.GetString(msg);
                if(str == "q")
                    break;
                //Console.WriteLine("Message: " + System.Text.Encoding.Default.GetString(msg));
                bytesSent = client.Send(msg);
                int bytesRec = client.Receive(bytes);
                Console.WriteLine("Client received: {0}",
                    Encoding.ASCII.GetString(bytes, 0, bytesRec));
            }

            client.Shutdown(SocketShutdown.Both);
        // }
        // catch
        return await Task.Run(() => { return 1; });
    }
}
    
