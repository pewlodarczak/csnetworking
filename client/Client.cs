﻿using System;
using System.Net.Sockets;
using System.Threading;

namespace SocketClient
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create a socket and connect to the server
            var client = new TcpClient();
            client.Connect("localhost", 8080);

            // Start a new thread for sending messages
            var sendThread = new Thread(() =>
            {
                while (true)
                {
                    // Read a message from the console
                    Console.WriteLine("Enter a message: ");
                    var message = Console.ReadLine();

                    // Send the message to the server
                    var stream = client.GetStream();
                    var data = System.Text.Encoding.ASCII.GetBytes(message);
                    stream.Write(data, 0, data.Length);
                }
            });
            sendThread.Start();

            // Start a new thread for receiving messages
            var receiveThread = new Thread(() =>
            {
                while (true)
                {
                    // Receive a message from the server
                    var stream = client.GetStream();
                    var data = new byte[1024];
                    var bytesReceived = stream.Read(data, 0, data.Length);
                    var message = System.Text.Encoding.ASCII.GetString(data, 0, bytesReceived);

                    // Print the message to the console
                    Console.WriteLine("Received: " + message);
                }
            });
            receiveThread.Start();
        }
    }
}
