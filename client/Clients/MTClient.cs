using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace SocketClient
{
    public class ProgramOld
    {
        // The IP address and port number of the server to connect to
        private const string ServerIP = "192.168.1.100";
        private const int ServerPort = 8000;

        // The message to send to the server
        private const string Message = "Hello, server!";

        // The maximum size of the message buffer
        private const int BufferSize = 1024;

        // The interval at which to send messages to the server (in milliseconds)
        private const int SendInterval = 1000;

        // The cancellation token used to stop the main thread
        private static CancellationTokenSource _cancellationTokenSource;

        public static void Start()
        {
            Console.WriteLine("Press Enter to start the client, or any other key to exit...");

            // Start the client if the user pressed Enter, otherwise exit
            if (Console.ReadKey(true).Key == ConsoleKey.Enter)
            {
                StartClient();
            }
            else
            {
                Environment.Exit(0);
            }
        }

        private static void StartClient()
        {
            // Create a new cancellation token source
            _cancellationTokenSource = new CancellationTokenSource();

            // Start the receive thread
            var receiveThread = new Thread(ReceiveMessages);
            receiveThread.Start();

            // Start the send thread
            var sendThread = new Thread(SendMessages);
            sendThread.Start();

            // Wait for the user to press any key to stop the client
            Console.WriteLine("Press any key to stop the client...");
            Console.ReadKey(true);

            // Cancel the cancellation token and wait for the threads to stop
            _cancellationTokenSource.Cancel();
            receiveThread.Join();
            sendThread.Join();
        }

        private static void ReceiveMessages()
        {
            // Create a new socket to connect to the server
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(new IPEndPoint(IPAddress.Parse(ServerIP), ServerPort));

            // Create a new byte array to store incoming messages
            var buffer = new byte[BufferSize];
            byte[] bytes = new byte[1024];
        
            while (!_cancellationTokenSource.IsCancellationRequested)
            {
                try{
                        int bytesRec = socket.Receive(bytes);
                        Console.WriteLine("Client received: {0}",
                            Encoding.ASCII.GetString(bytes, 0, bytesRec));

                }
                catch(Exception ex)
                {

                }
            }
        }

        private static void SendMessages()
        {
            // Create a new socket to connect to the server
            var socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            socket.Connect(new IPEndPoint(IPAddress.Parse(ServerIP), ServerPort));

            // Create a new byte array to store incoming messages
            var buffer = new byte[BufferSize];
            byte[] bytes = new byte[1024];
        
            while (!_cancellationTokenSource.IsCancellationRequested)
            {
                try{
                    Console.WriteLine("Enter message:");
                    byte[] msg = Encoding.ASCII.GetBytes(Console.ReadLine());
                    var str = System.Text.Encoding.Default.GetString(msg);
                    if(str == "q")
                        break;
                    int bytesSent = socket.Send(msg);

                }
                catch(Exception ex)
                {

                }
            }
        }


    }
}
